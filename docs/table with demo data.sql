USE [umbraco6beta]
GO
/****** Object:  Table [dbo].[ulocateGeocodedAddress]    Script Date: 2/7/2013 4:52:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ulocateGeocodedAddress](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NULL,
	[address1] [nvarchar](255) NULL,
	[address2] [nvarchar](255) NULL,
	[locality] [nvarchar](50) NULL,
	[region] [nvarchar](50) NULL,
	[postalCode] [nvarchar](50) NULL,
	[countryCode] [nvarchar](2) NULL,
	[comment] [nvarchar](500) NULL,
	[coordinateGeography] [geography] NULL,
	[viewportGeography] [geography] NULL,
	[geoCodeStatus] [nvarchar](20) NULL,
	[createDate] [datetime] NOT NULL,
	[updateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_uLocateAddress] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[ulocateGeocodedAddress] ON 

INSERT [dbo].[ulocateGeocodedAddress] ([id], [name], [address1], [address2], [locality], [region], [postalCode], [countryCode], [comment], [coordinateGeography], [viewportGeography], [geoCodeStatus], [createDate], [updateDate]) VALUES (8, N'Buckingham Palace', N'SW1A 1AA', N'', N'London', N'', N'', N'GB', NULL, 0xE6100000010C37001B1021C04940D772C2DF8A1FC2BF, 0xE61000000114382806ED7AC0494090AFF3250AE3C1BF7E0A3664F2BF49400835E8F0B50CC3BF, N'OK', CAST(0x0000A15300ED22EA AS DateTime), CAST(0x0000A15300ED22EA AS DateTime))
INSERT [dbo].[ulocateGeocodedAddress] ([id], [name], [address1], [address2], [locality], [region], [postalCode], [countryCode], [comment], [coordinateGeography], [viewportGeography], [geoCodeStatus], [createDate], [updateDate]) VALUES (11, N'Eiffel Tower', N'Champs-de-Mars', N'', N'Paris', N'', N'75007', N'FR', NULL, 0xE6100000010C3B922639BB6D4840F6ACB54137650240, 0xE6100000011487591E5A1A6E484077F52A323A700240535A7F4B006D4840E7D2AEF83C550240, N'OK', CAST(0x0000A15400AC9599 AS DateTime), CAST(0x0000A1540119E388 AS DateTime))
INSERT [dbo].[ulocateGeocodedAddress] ([id], [name], [address1], [address2], [locality], [region], [postalCode], [countryCode], [comment], [coordinateGeography], [viewportGeography], [geoCodeStatus], [createDate], [updateDate]) VALUES (20, N'Mandalay Bay Resort and Casino', N'3950 S Las Vegas Blvd', N'', N'Las Vegas', N'NV', N'89119', N'US', NULL, 0xE6100000010CB86F5A95E90B4240818CCA9761CB5CC0, 0xE61000000114A7E095C9150C424009D4AC7D4BCB5CC0C9FE1E61BD0B4240F844E8B177CB5CC0, N'OK', CAST(0x0000A15401297861 AS DateTime), CAST(0x0000A15401299481 AS DateTime))
INSERT [dbo].[ulocateGeocodedAddress] ([id], [name], [address1], [address2], [locality], [region], [postalCode], [countryCode], [comment], [coordinateGeography], [viewportGeography], [geoCodeStatus], [createDate], [updateDate]) VALUES (21, N'Disney World Dolphin Hotel', N'1500 Epcot Resorts Boulevard', N'', N'Lake Buena Vista', N'Florida', N'32830', N'US', NULL, 0xE6100000010C18247D5A455F3C40956E0157E86354C0, 0xE61000000114F705F4C29D5F3C401DB6E33CD26354C0394206F2EC5E3C400D271F71FE6354C0, N'OK', CAST(0x0000A154012A16B1 AS DateTime), CAST(0x0000A15600BDDF41 AS DateTime))
INSERT [dbo].[ulocateGeocodedAddress] ([id], [name], [address1], [address2], [locality], [region], [postalCode], [countryCode], [comment], [coordinateGeography], [viewportGeography], [geoCodeStatus], [createDate], [updateDate]) VALUES (24, N'Augustiner-Bräu', N'Landsberger Straße 31-35', N'', N'München', N'', N'80339', N'DE', NULL, 0xE6100000010CA01518B2BA1148406688635DDC162740, 0xE61000000114908653E6E6114840234C512E8D172740B1A4DC7D8E114840A8C4758C2B162740, N'OK', CAST(0x0000A1550020DC9E AS DateTime), CAST(0x0000A155002151E5 AS DateTime))
INSERT [dbo].[ulocateGeocodedAddress] ([id], [name], [address1], [address2], [locality], [region], [postalCode], [countryCode], [comment], [coordinateGeography], [viewportGeography], [geoCodeStatus], [createDate], [updateDate]) VALUES (27, N'Kenda Tire', N'7095 Americana Parkway', N'', N'Reynoldsburg', N'Ohio', N'43054', N'US', NULL, 0xE6100000010C3A26E6B402F743403CB94B87F3B354C0, 0xE61000000114DCF9D9232FF7434006C2A96ADDB354C092D739BCD6F64340F532E59E09B454C0, N'OK', CAST(0x0000A15500B73E00 AS DateTime), CAST(0x0000A15500B73E00 AS DateTime))
INSERT [dbo].[ulocateGeocodedAddress] ([id], [name], [address1], [address2], [locality], [region], [postalCode], [countryCode], [comment], [coordinateGeography], [viewportGeography], [geoCodeStatus], [createDate], [updateDate]) VALUES (28, N'Gateway Tire Batesville', N'1101 Hwy 49 South', N'', N'Clarksdale', N'MS', N'38615', N'US', NULL, 0xE6100000010C19DC7B5D2B3A414039B874CC799E56C0, 0xE61000000114382D78D1573A41408C5FC2B2639E56C05A4B0169FF394140B07092E68F9E56C0, N'OK', CAST(0x0000A15500B87BB5 AS DateTime), CAST(0x0000A15500B89DF7 AS DateTime))
INSERT [dbo].[ulocateGeocodedAddress] ([id], [name], [address1], [address2], [locality], [region], [postalCode], [countryCode], [comment], [coordinateGeography], [viewportGeography], [geoCodeStatus], [createDate], [updateDate]) VALUES (30, N'Gateway Tire Corinth', N'421 Hwy 72 West', N'', N'Corinth', N'MS', N'38834', N'US', NULL, 0xE6100000010CEBF0C63835774140007B3B78DC2256C0, 0xE610000001140270A24C5977414096D86B30C32256C0238E2BE4007741408649A764EF2256C0, N'OK', CAST(0x0000A15500B8E22C AS DateTime), CAST(0x0000A15500B8FFCC AS DateTime))
INSERT [dbo].[ulocateGeocodedAddress] ([id], [name], [address1], [address2], [locality], [region], [postalCode], [countryCode], [comment], [coordinateGeography], [viewportGeography], [geoCodeStatus], [createDate], [updateDate]) VALUES (32, N'Washington State Convention Center', N'800 Convention Pl', N'', N'Seattle', N'WA', N'98101', N'US', NULL, 0xE6100000010C2C11A8FE41CE4740302FC03E3A955EC0, 0xE610000001141B82E3326ECE4740B876A22424955EC03CA06CCA15CE4740A7E7DD5850955EC0, N'OK', CAST(0x0000A1560109DF97 AS DateTime), CAST(0x0000A1560109DF97 AS DateTime))
INSERT [dbo].[ulocateGeocodedAddress] ([id], [name], [address1], [address2], [locality], [region], [postalCode], [countryCode], [comment], [coordinateGeography], [viewportGeography], [geoCodeStatus], [createDate], [updateDate]) VALUES (34, N'Mindfly Web Design Studio', N'114 W. Magnolia St', N'Suite 504', N'Bellingham', N'WA', N'98225', N'US', N'360-647-7470', 0xE6100000010C653909A52F60484070067FBF989E5EC0, 0xE6100000011454AA44D95B604840F94D61A5829E5EC075C8CD7003604840E8BE9CD9AE9E5EC0, N'OK', CAST(0x0000A15E00FFE542 AS DateTime), CAST(0x0000A15E00FFFC48 AS DateTime))
SET IDENTITY_INSERT [dbo].[ulocateGeocodedAddress] OFF
ALTER TABLE [dbo].[ulocateGeocodedAddress] ADD  CONSTRAINT [DF_ulocateAddress_address2]  DEFAULT (NULL) FOR [address2]
GO
ALTER TABLE [dbo].[ulocateGeocodedAddress] ADD  CONSTRAINT [DF_ulocateAddress_coordinateGeography]  DEFAULT (NULL) FOR [coordinateGeography]
GO
ALTER TABLE [dbo].[ulocateGeocodedAddress] ADD  CONSTRAINT [DF_ulocateAddress_viewportGeography]  DEFAULT (NULL) FOR [viewportGeography]
GO
ALTER TABLE [dbo].[ulocateGeocodedAddress] ADD  CONSTRAINT [DF_ulocateAddress_createDate]  DEFAULT (getdate()) FOR [createDate]
GO
ALTER TABLE [dbo].[ulocateGeocodedAddress] ADD  CONSTRAINT [DF_ulocateAddress_updateDate]  DEFAULT (getdate()) FOR [updateDate]
GO
