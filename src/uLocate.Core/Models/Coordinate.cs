﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace uLocate.Core.Models
{
    [Serializable]
    [DataContract(IsReference = true)]
    public class Coordinate : ICoordinate
    {
        private double _latitude;
        private double _longitude;

        /// <summary>
        /// The Latitude value
        /// </summary>
        [DataMember(Name = "latitude")]
        public double Latitude
        {
            get { return _latitude; }
            private set { }
        }

        /// <summary>
        /// The Longitude value
        /// </summary>
        [DataMember(Name = "longitude")]
        public double Longitude
        {
            get { return _longitude; }
            private set { }
        }

        #region Constructor

        /// <summary>
        /// Constructor for Coordinate
        /// </summary>
        /// <param name="latitude">The latitude value</param>
        /// <param name="longitude">The longitude value</param>
        public Coordinate(double latitude, double longitude)
        {
            _latitude = latitude;
            _longitude = longitude;
        }

        #endregion

        public override string ToString()
        {
            return string.Join(",", new[] { this.Latitude, this.Longitude });
        }
    }
}
