﻿using System;
using System.Collections.Generic;
using System.Deployment.Internal;
using System.Linq;
using System.Text;
using Umbraco.Core;
using Umbraco.Core.Models.EntityBase;

namespace uLocate.Core.Models.EntityBase
{
    public class Entity : IEntity
    {
        public int Id { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdateDate { get; set; }

        public Guid Key
        {
            get
            {
                return Id.ToGuid();
            }
            set
            {
               
            }
        }

        public bool HasIdentity { get; set; }

        /// <summary>
        /// Method to call on entity saved when first added
        /// </summary>
        internal virtual void AddingEntity()
        {
            CreateDate = DateTime.Now;
            UpdateDate = DateTime.Now;
        }

        /// <summary>
        /// Method to call on entity saved/updated
        /// </summary>
        internal virtual void UpdatingEntity()
        {
            UpdateDate = DateTime.Now;
        }






    }
}
