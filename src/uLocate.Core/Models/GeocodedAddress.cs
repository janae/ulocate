﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using uLocate.Core.Models.EntityBase;


namespace uLocate.Core.Models
{
    [Serializable]
    [DataContract(IsReference = true)]
    public class GeocodedAddress : Entity, IGeocodedAddress
    {
        private string _name;
        private string _address1;
        private string _address2;
        private string _locality;
        private string _region;
        private string _postalCode;
        private string _countryCode;
        private string _comment;
        private GeocodeStatus _geocodeStatus;

        #region Implementation of IAddress

        /// <summary>
        /// The name of the address
        /// </summary>
        [DataMember]
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
            }
        }

        /// <summary>
        /// Address line 1
        /// </summary>
        [DataMember]
        public string Address1
        {
            get { return _address1; }
            set
            {
                _address1 = value;
            }
        }

        /// <summary>
        /// Address line 2
        /// </summary>
        [DataMember]
        public string Address2
        {
            get { return _address2; }
            set
            {
                _address2 = value;
            }
        }

        /// <summary>
        /// The locality or city of the address
        /// </summary>
        [DataMember]
        public string Locality
        {
            get { return _locality; }
            set
            {
                _locality = value;
            }
        }

        /// <summary>
        /// The region/state/province of the address
        /// </summary>
        [DataMember]
        public string Region
        {
            get { return _region; }
            set
            {
                _region = value;
            }
        }

        /// <summary>
        /// The postal code of the address
        /// </summary>
        [DataMember]
        public string PostalCode
        {
            get { return _postalCode; }
            set
            {
                _postalCode = value;
            }
        }

        /// <summary>
        /// The country code associated with the address
        /// </summary>
        [DataMember]
        public string CountryCode
        {
            get { return _countryCode; }
            set
            {
                _countryCode = value;

            }
        }

        #endregion

        #region Implementation of IGeocodedAddress

        // I don't want Coordinate or Viewport to mark the object as IsDirty() as it would be geocoded again and reset.

        /// <summary>
        /// The <see cref="Coordinate"/> of the address
        /// </summary>
        [DataMember]
        public Coordinate Coordinate { get; set; }

        /// <summary>
        /// Recommended bounding box to center and zoom for this result.  <see cref="Viewport"/>
        /// </summary>
        [DataMember]
        public Viewport Viewport { get; set; }

        /// <summary>
        /// The <see cref="GeocodeStatus"/> of the Geocode
        /// </summary>
        [DataMember]
        public GeocodeStatus GeocodeStatus
        {
            get { return _geocodeStatus; }
            set
            {
                var preValue = _geocodeStatus;
                _geocodeStatus = value;
                //if(preValue != _geocodeStatus) OnPropertyChanged(this.GetType().GetProperty("GeocodeStatus"));
            }
        }

        [DataMember]
        public string Comment
        {
            get { return _comment; }
            set
            {                
                _comment = value;
            }
        }

        [DataMember]
        public string Email
        {
            get { return ExtendedDataValue("Email"); }
            set { SaveExtendedData("Email", value); }
        }

        [DataMember]
        public string Telephone
        {
            get { return ExtendedDataValue("Telephone"); }
            set { SaveExtendedData("Telephone", value); }
        }

        [DataMember]
        public string Fax
        {
            get { return ExtendedDataValue("Fax"); }
            set { SaveExtendedData("Fax", value); }
        }

        [DataMember]
        public string WebsiteUrl
        {
            get { return ExtendedDataValue("WebsiteUrl"); }
            set { SaveExtendedData("WebsiteUrl", value); }

        }

        [IgnoreDataMember]
        public IDictionary<string, string> ExtendedData { get; internal set; }

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public GeocodedAddress()
        {            
            Coordinate = new Coordinate(0, 0);
            GeocodeStatus = GeocodeStatus.NOT_QUERIED;
            ExtendedData = new Dictionary<string, string>();
        }

        public GeocodedAddress(IDictionary<string, string> extendedData)
        {
            Coordinate = new Coordinate(0, 0);
            GeocodeStatus = GeocodeStatus.NOT_QUERIED;
            ExtendedData = extendedData;
        }

        /// <summary>
        /// Retrieves a value from the extended data dictionary
        /// </summary>
        /// <param name="key">Dictionary key</param>
        /// <returns>The value</returns>
        public string ExtendedDataValue(string key)
        {
            return ExtendedData.ContainsKey(key) ? ExtendedData[key] : string.Empty;
        }

        /// <summary>
        /// Saves a value from the extended data dictionary
        /// </summary>
        /// <param name="key">Dictionary key</param>
        /// <param name="value"></param>
        /// <returns>The value</returns>
        public void SaveExtendedData(string key, string value)
        {
            if (!ExtendedData.ContainsKey(key))
            {
                
                ExtendedData.Add(key, value);
            }
            else
            {                
                ExtendedData[key] = value;
            }
        }

        public override string ToString()
        {
            return Geocode.FormatAddress(this);
        }

        /// <summary>
        /// True/false indicating whether or not the address properties can be formatted for geocoding
        /// </summary>
        public bool AddressCanBeFormatted()
        {
            return !string.IsNullOrEmpty(Geocode.FormatAddress(this));
        }
    }
}