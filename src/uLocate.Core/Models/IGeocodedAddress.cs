﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Umbraco.Core.Models.EntityBase;


namespace uLocate.Core.Models
{
    public interface IGeocodedAddress : IAddress, IEntity
    {

        /// <summary>
        /// The <see cref="Coordinate"/> of the address
        /// </summary>
        Coordinate Coordinate { get; set; }

        /// <summary>
        /// Recommended bounding box to center and zoom for this result
        /// </summary>
        Viewport Viewport { get; set; }

        /// <summary>
        /// The <see cref="GeocodeStatus"/> of the Geocode
        /// </summary>
        GeocodeStatus GeocodeStatus { get; set; }

        /// <summary>
        /// A comment or description
        /// </summary>
        string Comment { get; set; }

        /// <summary>
        /// An email address associated with this address
        /// </summary>
        /// <remarks>
        /// Persisted in ExtendedDataXml
        /// </remarks>
        string Email { get; set; }

        /// <summary>
        /// A telephone number associated with this address
        /// </summary>
        /// <remarks>
        /// Persisted in ExtendedDataXml
        /// </remarks>
        string Telephone { get; set; }

        /// <summary>
        /// A telephone number associated with this address
        /// </summary>
        /// <remarks>
        /// Persisted in ExtendedDataXml
        /// </remarks>
        string Fax { get; set; }

        /// <summary>
        /// A website URL associated with this addrress
        /// </summary>
        /// <remarks>
        /// Persisted in ExtendedDataXml
        /// </remarks>
        string WebsiteUrl { get; set; }
 
        /// <summary>
        /// Additional custom data
        /// </summary>
        /// <remarks>
        /// Persisted in ExtendedDataXml
        /// </remarks>
        IDictionary<string, string> ExtendedData { get; }

        /// <summary>
        /// Indicates whether or not the address can be formated for geocoding.
        /// </summary>
        /// <returns></returns>
        bool AddressCanBeFormatted();

    }
}
