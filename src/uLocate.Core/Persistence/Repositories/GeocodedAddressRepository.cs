﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Persistence;
using uLocate.Core.Models;
using uLocate.Core.Models.Rdbms;
using uLocate.Core.Persistence.Factories;
using Umbraco.Core.Persistence.Repositories;
using Umbraco.Core.Persistence.UnitOfWork;

namespace uLocate.Core.Persistence.Repositories
{
    internal class GeocodedAddressRepository : RepositoryAbstract<int, GeocodedAddress>, IGeocodedAddressRepository
    {

        public GeocodedAddressRepository(IDatabaseUnitOfWork work)
            : base(work)
        {
        }

        /// <summary>
        /// Returns the database Unit of Work added to the repository
        /// </summary>
        protected internal new IDatabaseUnitOfWork UnitOfWork
        {
            get { return (IDatabaseUnitOfWork)base.UnitOfWork; }
        }

        protected UmbracoDatabase Database
        {
            get { return UnitOfWork.Database; }
        }
        protected override GeocodedAddress PerformGet(int id)
        {
            var sql = GetBaseQuery(false);
            sql.Where(GetBaseWhereClause(), new { Id = id });

            var dto = Database.Fetch<GeocodedAddressDto>(sql).FirstOrDefault();

            if (dto == null)
                return null;

            var factory = new GeocodedAddressFactory();

            return factory.BuildAddress(dto);
        }

        protected override IEnumerable<GeocodedAddress> PerformGetAll(params int[] ids)
        {
            if (ids.Any())
            {
                foreach (var id in ids)
                {
                    yield return Get(id);
                }
            }
            else
            {
                var factory = new GeocodedAddressFactory();
                var dtos = Database.Fetch<GeocodedAddressDto>(GetBaseQuery(false));
                foreach (var dto in dtos)
                {
                    yield return factory.BuildAddress(dto);
                }
            }
        }


        protected override void PersistNewItem(GeocodedAddress entity)
        {
            entity.AddingEntity();

            var factory = new GeocodedAddressFactory();
            var dto = factory.BuildDto(entity);
            entity.Id = int.Parse(Database.Insert(dto).ToString());
            
        }

        protected override void PersistUpdatedItem(GeocodedAddress entity)
        {
            entity.UpdatingEntity();

            var factory = new GeocodedAddressFactory();
            var dto = factory.BuildDto(entity);

            Database.Update(dto);

        }

        protected override void PersisteDeletedItem(GeocodedAddress entity)
        {
            var deletes = GetDeleteClauses();
            foreach (var delete in deletes)
            {
                Database.Execute(delete, new {Id = entity.Id});
            }

        }

        public override void Delete(GeocodedAddress entity)
        {
            
            PersistDeletedItem(entity);            
        }
    
        protected override bool PerformExists(int id)
        {
            var sql = GetBaseQuery(true);
            sql.Where(GetBaseWhereClause(), new { Id = id });
            var count = Database.ExecuteScalar<int>(sql);
            return count == 1;
        }

        protected override int PerformCount(Sql sql)
        {
            return Database.ExecuteScalar<int>(sql);
        }

        protected override Sql GetBaseQuery(bool isCount)
        {
            var sql = new Sql();
            sql.Select(isCount ? "COUNT(*)" : "*").From<GeocodedAddressDto>();
            return sql;
        }

        protected override string GetBaseWhereClause()
        {
            return "ulocateGeocodedAddress.id = @Id";
        }

        protected override IEnumerable<string> GetDeleteClauses()
        {
            return new List<string> { "DELETE FROM ulocateGeocodedAddress WHERE id = @Id" };
        }




    }
}