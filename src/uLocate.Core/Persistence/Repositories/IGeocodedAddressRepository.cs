﻿using uLocate.Core.Models;
using Umbraco.Core.Persistence.Repositories;

namespace uLocate.Core.Persistence.Repositories
{
    public interface IGeocodedAddressRepository : IRepository<int, GeocodedAddress>
    {
    }
}
