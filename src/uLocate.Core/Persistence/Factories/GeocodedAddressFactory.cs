﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Types;
using uLocate.Core.Models;
using uLocate.Core.Models.Rdbms;
using uLocate.Core.Extensions;
using System.Runtime.Serialization;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace uLocate.Core.Persistence.Factories
{
    public class GeocodedAddressFactory
    {
       
        /// <summary>
        /// Builds a <see cref="GeocodedAddress"/> from an <see cref="GeocodedAddressDto"/>
        /// </summary>
        /// <returns><see cref="GeocodedAddress"/></returns>
        public GeocodedAddress BuildAddress(GeocodedAddressDto dto)
        {
            var extended = DeserializeExtendedDataXml(dto.ExtendedDataXml);

            var address = new GeocodedAddress()
                {
                    Id = dto.Id,
                    Name = dto.Name,
                    Address1 = dto.Address1,
                    Address2 = dto.Address2,
                    Locality = dto.Locality,
                    Region = dto.Region,
                    PostalCode = dto.PostalCode,
                    CountryCode = dto.CountryCode,
                    Comment = !string.IsNullOrEmpty(dto.Comment) ? dto.Comment : string.Empty,
                    Coordinate = dto.CoordinateGeography.ToCoordinate(),
                    Viewport = new Viewport(dto.ViewportGeography),
                    GeocodeStatus = (GeocodeStatus)Enum.Parse(typeof(GeocodeStatus), dto.GeoCodeStatus),    
                    ExtendedData = DeserializeExtendedDataXml(dto.ExtendedDataXml),
                    UpdateDate = dto.UpdateDate,
                    CreateDate = dto.CreateDate
                };

            return address;
        }
        
        /// <summary>
        /// Builds an <see cref="GeocodedAddressDto"/> from a <see cref="GeocodedAddress"/>
        /// </summary>
        /// <returns><see cref="GeocodedAddressDto"/></returns>
        public GeocodedAddressDto BuildDto(IGeocodedAddress geoAddress)
        {
            var dto = new GeocodedAddressDto()
                {
                    Id = geoAddress.Id,
                    Name = geoAddress.Name,
                    Address1 = geoAddress.Address1,
                    Address2 = geoAddress.Address2,
                    Locality = geoAddress.Locality,
                    Region = geoAddress.Region,
                    PostalCode = geoAddress.PostalCode,
                    CountryCode = geoAddress.CountryCode,
                    Comment = geoAddress.Comment,
                    CoordinateGeography = geoAddress.Coordinate.ToSqlGeography(),
                    ViewportGeography = geoAddress.Viewport.SqlGeography,
                    GeoCodeStatus = geoAddress.GeocodeStatus.ToString(),      
                    ExtendedDataXml = SerializeExtendedDataXml(geoAddress),
                    CreateDate = geoAddress.CreateDate,
                    UpdateDate = geoAddress.UpdateDate
                };
            return dto;
        }

        /// <summary>
        /// Translates saved XML data into <see cref="Dictionary<string, string>"/>
        /// </summary>
        /// <param name="xml">ExtendedData Xml</param>
        /// <returns><see cref="Dictionary<string, string>"/></returns>
        private static Dictionary<string, string> DeserializeExtendedDataXml(string xml)
        {
            var dictionary = new Dictionary<string, string>();

            if (string.IsNullOrEmpty(xml)) return dictionary;

            XDocument doc = XDocument.Parse(xml);

            foreach (XElement el in doc.Element("ExtendedData").Elements())
            {
                dictionary.Add(el.Name.LocalName, el.Value);
            }
            return dictionary;
        }

        /// <summary>
        /// Serializes the ExtendedData dictionary to an xml string
        /// </summary>
        /// <param name="geoAddress"><see cref="GeocodedAddress"/</param>
        /// <returns>Extended data dictionary serialized as xml</returns>
        private static string SerializeExtendedDataXml(IGeocodedAddress geoAddress)
        {
            
            var keyAsserts = new HashSet<string>();

            string xml;
            using (var sw = new StringWriter())
            {
                using (var writer = new XmlTextWriter(sw))
                {

                    writer.WriteStartDocument();
                    
                    writer.WriteStartElement("ExtendedData");
                    
                    foreach (var item in geoAddress.ExtendedData.Keys)
                    {
                        if(!keyAsserts.Contains(XmlConvert.EncodeLocalName(item)))
                        {
                            keyAsserts.Add(XmlConvert.EncodeLocalName(item));
                            writer.WriteElementString(XmlConvert.EncodeLocalName(item), geoAddress.ExtendedData[item]);                            
                        }
                        else
                        {
                            string msg = "Cannot serialize dictionary key " + item + " as it would create a duplicate";  
                            var ex = new Exception(msg);
                            Umbraco.Core.Logging.LogHelper.Error(typeof(GeocodedAddressFactory), msg, ex);
                            throw ex;
                        }
                    }
                    
                    writer.WriteEndElement(); // ExtendedData
                    
                    writer.WriteEndDocument();

                    xml = sw.ToString();
                }
            }

            return xml;
        }
    }
}
