﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web;
using System.Xml;
using uLocate.Core.Configuration;
using uLocate.Core.Models;

namespace uLocate.Core.Geocoding
{
    [System.Obsolete("The Geocoding API V2 has been deprecated by Google.  Use the GoogleMapsServiceProviderV3")]
    public class GoogleMapsServiceProvider : AbstractGeocodeServiceProvider  
    {
        /// <summary>
        /// GoogleMapsServiceProvider constructor
        /// </summary>
        public GoogleMapsServiceProvider()
        {
            var config = Constants.Configuration.Providers["GeocodingAPIProvider"];

            // configure the provider
            GoogleMapsServiceProvider.EnableCaching = config.EnableCaching;
            GoogleMapsServiceProvider.CacheDuration = config.CacheDuration;
            GoogleMapsServiceProvider.LogRequests = config.LogRequests; // not used

            Settings = new Dictionary<string, string>();

            foreach (SettingElement setting in config.Settings)
            {
                Settings.Add(setting.Name, setting.Value);
            }

            //if (!Settings.ContainsKey("APIKey")) Settings.Add("APIKey", string.Empty);
        }

        /// <summary>
        /// Queries the Google Geocoding API for the formattedAddress
        /// </summary>
        public override IGeocodeResponse GetGeocodeResponse(string addressString)
        {
            return GetGeocodeResponse(addressString, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
        }

        /// <summary>
        /// Queries the Google Geocoding API for a geocode
        /// </summary>
        /// <param name="address">The street address value</param>
        /// <param name="locality">The locality or city value</param>
        /// <param name="region">The region or state value</param>
        /// <param name="postalCode">The postal code value</param>
        /// <returns></returns>
        public override IGeocodeResponse GetGeocodeResponse(string address1, string address2, string locality, string region, string postalCode, string countryCode)
        {
            // format the query from the parameters passed
            var q = HttpUtility.UrlEncode(Geocode.FormatAddress(address1, address2, locality, region, postalCode, countryCode));

            // get the url from the configuration
            var url = string.Format(Settings["UrlString"], q);

            //var apiKey = Settings["APIKey"];

            //if (!string.IsNullOrEmpty(apiKey)) url += string.Format("&key={0}", apiKey);

            // create a cacheKey
            var cacheKey = string.Concat("uLocate:GeocodeResponse||", url);

            if (EnableCaching && Cache[cacheKey] != null)
            {
                // this has recently been queried.  used the cached value
                return (GeocodeResponse)Cache[cacheKey];
            }
            else
            {
                // the actual web request to the geocode api
                var req = (HttpWebRequest)WebRequest.Create(url);
                using (var resp = (HttpWebResponse)req.GetResponse())
                {
                    using (var sr = new StreamReader(resp.GetResponseStream()))
                    {
                        // query the api for the response and cache the data
                        var gr = CreateResponse(sr.ReadToEnd());
                        CacheData(cacheKey, gr);
                        return gr;
                    }
                }
            }
        }

        #region Helpers

        /// <summary>
        /// Parses the xml returned from Google Geocoding API
        /// </summary>
        /// <param name="xml">API results xml</param>
        /// <returns>GeocodeResponse with values from the API results returned</returns>
        private GeocodeResponse CreateResponse(string xml)
        {
            GeocodeStatus _status;
            List<Geocode> geocodes = new List<Geocode>();

            // Load the xml response
            XmlDocument apiResponse = new XmlDocument();
            apiResponse.LoadXml(xml);

            // Get the namespace manager - "maps" is the prefix
            XmlNamespaceManager nsmgr = GetXmlNamespaceManager(apiResponse);

            // set the status
            //TODO flush out other response codes
            XmlNode response = apiResponse.DocumentElement.SelectSingleNode("maps:Response", nsmgr);

            switch (response.SelectSingleNode("maps:Status/maps:code", nsmgr).InnerText)
            {
                case "200":
                    _status = GeocodeStatus.OK;
                    break;
                default:
                    _status = GeocodeStatus.INVALID_REQUEST;
                    break;
            }

            //if (response.SelectSingleNode("maps:Status/maps:code", nsmgr).InnerText == "200")
            //{
            //    _status = GeocodeStatus.OK;
            //}
            //else
            //{
            //    _status = GeocodeStatus.INVALID_REQUEST;
            //}
            
            // create the list of resulting geocodes
            if (_status == GeocodeStatus.OK)
            {
                // create the collection of results
                var placemarks = response.SelectNodes("maps:Placemark", nsmgr);
                foreach (XmlNode result in placemarks)
                {
                    string formattedAddress = result.SelectSingleNode("maps:address", nsmgr).InnerText;
                    
                    // quality interpretation
                    GeocodeQuality quality;
                    switch (int.Parse(result.SelectSingleNode("oasis:AddressDetails", nsmgr).Attributes["Accuracy"].Value))
                    {
                        case 4 :
                            quality = GeocodeQuality.GEOMETRIC_CENTER;
                            break;
                        case 5 :
                            quality = GeocodeQuality.GEOMETRIC_CENTER;
                            break;
                        case 6 :
                            quality = GeocodeQuality.RANGE_INTERPOLATED;
                            break;
                        case 7 :
                            quality = GeocodeQuality.RANGE_INTERPOLATED;
                            break;
                        case 8 :
                            quality = GeocodeQuality.ROOFTOP;
                            break;
                        case 9 :
                            quality = GeocodeQuality.ROOFTOP;
                            break;
                        default :
                            quality = GeocodeQuality.APPROXIMATE;
                            break;
                    }

                    // Get the Latitude and Longitude and the viewport
                    // Lat/Long
                    string[] coords = result.SelectSingleNode("maps:Point/maps:coordinates", nsmgr).InnerText.Split(',');
                    double latitude = double.Parse(coords[1]);
                    double longitude = double.Parse(coords[0]);

                    // Viewport
                    
                    XmlNode vpbox = result.SelectSingleNode("maps:ExtendedData/maps:LatLonBox", nsmgr);

                    var northEast = new Coordinate(
                            double.Parse(vpbox.Attributes["north"].Value), 
                            double.Parse(vpbox.Attributes["east"].Value));

                    var southWest = new Coordinate(
                            double.Parse(vpbox.Attributes["south"].Value),
                            double.Parse(vpbox.Attributes["west"].Value));

                    Viewport vp = new Viewport(southWest, northEast);

                    Geocode g = new Geocode(formattedAddress, latitude, longitude, quality, vp);

                    // add the geocode to the result
                    geocodes.Add(g);
                }                               
            } // response ok


            // prepare the response
            GeocodeResponse resp = new GeocodeResponse(_status, geocodes);

            // return the response
            return resp;
        }

        /// <summary>
        /// Helper - creates an XmlNamespaceManager for the Google Geocoding API xml results
        /// </summary>      
        private static XmlNamespaceManager GetXmlNamespaceManager(XmlDocument doc)
        {
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("maps", "http://earth.google.com/kml/2.0");
            nsmgr.AddNamespace("oasis", "urn:oasis:names:tc:ciq:xsdschema:xAL:2.0");
            return nsmgr;
        }

        #endregion
    }
}