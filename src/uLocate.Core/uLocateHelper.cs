﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using uLocate.Core.Geocoding;
using uLocate.Core.Models;
using uLocate.Core.Persistence.Repositories;
using uLocate.Core.Services;

namespace uLocate.Core
{
    public class ULocateHelper
    {
        /// <summary>
        /// Returns an instantiation of the geocoding service provider configured
        /// </summary>
        public static AbstractGeocodeServiceProvider GeocodingService()
        {
            return AbstractGeocodeServiceProvider.Instance();
        }

        public static IGeocodedAddress GeocodedAddress(int id)
        {
            var service = new GeocodedAddressService();
            return service.GetById(id);
        }

        public static IEnumerable<IGeocodedAddress> GeocodedAddressList()
        {
            var service = new GeocodedAddressService();

            return service.GetAll();
        }


        /// <summary>
        /// Calculates a recommended viewport based on addresses found
        /// </summary>
        /// <param name="addresses">A list of geocoded addresses</param>
        /// <returns><see cref="Viewport"/></returns>
        public static Viewport CalculateViewport(IEnumerable<GeocodedAddress> addresses)
        {
            // find min sw / max ne
            Viewport best = null;
            if (addresses.Any())
            {
                var minSwLat = addresses.First().Viewport.SouthWest.Latitude;
                var minSwLong = addresses.First().Viewport.SouthWest.Longitude;
                var maxNeLat = addresses.First().Viewport.NorthEast.Latitude;
                var maxNeLong = addresses.First().Viewport.NorthEast.Longitude;

                foreach (var adr in addresses)
                {
                    minSwLat = adr.Viewport.SouthWest.Latitude < minSwLat ? adr.Viewport.SouthWest.Latitude : minSwLat;
                    minSwLong = adr.Viewport.SouthWest.Longitude < minSwLong ? adr.Viewport.SouthWest.Longitude : minSwLong;
                    maxNeLat = adr.Viewport.NorthEast.Latitude > maxNeLat ? adr.Viewport.NorthEast.Latitude : maxNeLat;
                    maxNeLong = adr.Viewport.NorthEast.Longitude > maxNeLong ? adr.Viewport.NorthEast.Longitude : maxNeLong;
                }

                best = new Viewport(new Coordinate(minSwLat, minSwLong), new Coordinate(maxNeLat, maxNeLong));
            }

            return best ?? new Viewport(new Coordinate(0, 0), new Coordinate(1, 1));
        }

        public static IDictionary<string, string> CountryCodes()
        {
            var dict = new Dictionary<string, string>();

            foreach (var culture in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
            {
                var regionInfo = new RegionInfo(culture.Name);
                if (!dict.ContainsKey(regionInfo.EnglishName))
                {
                    dict.Add(regionInfo.EnglishName, regionInfo.TwoLetterISORegionName.ToUpper());
                }
            }

            var sorted = new SortedList<string, string>(dict);

            return sorted;
        }

        public static IGeocodedAddress LookupGeocode(string address)
        {
            var service = new GeocodedAddressService();

            var geoAddress = new GeocodedAddress() { Address1 = address };

            geoAddress = (GeocodedAddress)service.LookupGeocode(geoAddress);

            return geoAddress;
        }
    }
}