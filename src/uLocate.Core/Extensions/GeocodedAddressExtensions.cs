﻿using System;
using System.Collections.Generic;
using uLocate.Core.Models;
using uLocate.Core.Persistence.Repositories;
using uLocate.Core.Services;
using Umbraco.Core.Logging;

namespace uLocate.Core.Extensions
{
    public static class GeocodedAddressExtensions
    {
        /// <summary>
        /// Saves the address and performs a geocode query if new or properties changed
        /// </summary>
        /// <param name="geoAddress"><see cref="GeocodedAddress"/></param>
        public static void Save(this GeocodedAddress geoAddress)
        {
      
                if (geoAddress.GeocodeStatus != GeocodeStatus.OK)
                    geoAddress.LookupGeocode();

                try
                {
                    var service = new GeocodedAddressService();
                    service.Save(geoAddress);
                    
                }
                catch (Exception ex)
                {
                    LogHelper.Error<GeocodedAddressRepository>("An error occurred trying to Save", ex);
                }
            
        }

        /// <summary>
        /// True/false indicating whether or not the address properties can be formatted for geocoding
        /// </summary>
        public static bool AddressCanBeFormatted(this GeocodedAddress geoAddress)
        {
            return !string.IsNullOrEmpty(Geocode.FormatAddress(geoAddress));
        }

        /// <summary>
        /// Deletes the address
        /// </summary>
        /// <param name="geoAddress"><see cref="GeocodedAddress"/></param>
        public static void Delete(this GeocodedAddress geoAddress)
        {
            var service = new GeocodedAddressService();

            service.Delete(geoAddress);
        }

        /// <summary>
        /// Performs the Geocode query against configured provider.
        /// This is called automatically by the Save() extension.
        /// </summary>
        /// <param name="geoAddress"><see cref="GeocodedAddress" /></param>
        public static void LookupGeocode(this GeocodedAddress geoAddress)
        {

            var service = new GeocodedAddressService();
            geoAddress = (GeocodedAddress)service.LookupGeocode(geoAddress);
        }

        public static void LookupGeocodes(this IEnumerable<GeocodedAddress> geoAddresses)
        {
            var service = new GeocodedAddressService();

            geoAddresses = (IEnumerable<GeocodedAddress>)service.LookupGeocode(geoAddresses);

        }

        #region Distance

        /// <summary>
        /// Extension of STDistance - converts calculated distance to Kilometers
        /// </summary>
        public static double DistanceKilometers(this GeocodedAddress geoAddress, GeocodedAddress other)
        {
            return geoAddress.Coordinate.ToSqlGeography().DistanceKilometers(other.Coordinate.ToSqlGeography());
        }

        public static double DistanceKilometers(this GeocodedAddress geoAddress, double latitude, double longitude)
        {
            var coordinate = new Coordinate(latitude, longitude);
            return geoAddress.Coordinate.ToSqlGeography().DistanceKilometers(coordinate.ToSqlGeography());
        }

        /// <summary>
        /// Extension of STDistance - converts calculated distance to Miles
        /// </summary>
        public static double DistanceMiles(this GeocodedAddress geoAddress, GeocodedAddress other)
        {
            return geoAddress.Coordinate.ToSqlGeography().DistanceMiles(other.Coordinate.ToSqlGeography());
        }

        public static double DistanceMiles(this GeocodedAddress geoAddress, double latitude, double longitude)
        {
            var coordinate = new Coordinate(latitude, longitude);
            return geoAddress.Coordinate.ToSqlGeography().DistanceMiles(coordinate.ToSqlGeography());
        }

        #endregion
    }
}
