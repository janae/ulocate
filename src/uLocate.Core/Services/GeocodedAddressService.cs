﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Lucene.Net.Store;
using uLocate.Core.Geocoding;
using uLocate.Core.Persistence;

using uLocate.Core.Models;
using uLocate.Core.Events;
using Umbraco.Core;
using Umbraco.Core.Persistence.UnitOfWork;

namespace uLocate.Core.Services
{
    public class GeocodedAddressService : IGeocodedAddressService
    {
        private readonly IDatabaseUnitOfWorkProvider _uowProvider;
        private readonly RepositoryFactory _repositoryFactory;

        private static readonly ReaderWriterLockSlim Locker = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);

        #region Constructors


        public GeocodedAddressService()
            : this(new RepositoryFactory())
        {
            
        }

        public GeocodedAddressService(RepositoryFactory repositoryFactory)
            : this(new PetaPocoUnitOfWorkProvider(), repositoryFactory)
        {
            
        }

        public GeocodedAddressService(IDatabaseUnitOfWorkProvider provider, RepositoryFactory repositoryFactory)
        {
            if(provider == null) throw new ArgumentNullException("provider", "The IDatabaseUnitOfWorkProvider cannot be null");
            if (repositoryFactory == null) throw new ArgumentNullException("repositoryFactory", "The RepositoryFactory cannot be null");

            _uowProvider = provider;
            _repositoryFactory = repositoryFactory;

        }

        #endregion

        /// <summary>
        /// Creates a <see cref="IGeocodedAddress"/> object
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public IGeocodedAddress CreateGeocodedAddress(string name)
        {
            var address = new GeocodedAddress()
            {
                Name = name,
                HasIdentity =  false

            };

            Created.RaiseEvent(new NewEventArgs<IGeocodedAddress>(address), this);

            return address;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IGeocodedAddress> GetAll()
        {
            using (var repository = _repositoryFactory.CreateGeocodedAddressRepository(_uowProvider.GetUnitOfWork()))
            {
                return repository.GetAll();
            }
        }

        public IGeocodedAddress GetById(int id)
        {
            using (var repository = _repositoryFactory.CreateGeocodedAddressRepository(_uowProvider.GetUnitOfWork()))
            {
                return repository.Get(id);
            }
        }

        /// <summary>
        /// Saves a single GeocodedAddress
        /// </summary>
        /// <param name="address"></param>
        /// <param name="forceGeocodingIfAlreadyPresent"></param>
        public void Save(IGeocodedAddress address, bool forceGeocodingIfAlreadyPresent = false)
        {
            Saving.RaiseEvent(new SaveEventArgs<IGeocodedAddress>(address), this);

            using (new WriteLock(Locker))
            {
                var uow = _uowProvider.GetUnitOfWork();

                using (var repository = _repositoryFactory.CreateGeocodedAddressRepository(uow))
                {
                    repository.AddOrUpdate((GeocodedAddress)LookupGeocode(address, forceGeocodingIfAlreadyPresent));
                    uow.Commit();
                }
            }

            Saved.RaiseEvent(new SaveEventArgs<IGeocodedAddress>(address), this);

        }

        public void Save(IEnumerable<IGeocodedAddress> addresses, bool forceGeocodingIfAlreadyPresent = false)
        {

            addresses = LookupGeocode(addresses, forceGeocodingIfAlreadyPresent);

            var geocodedAddresses = addresses as IGeocodedAddress[] ?? addresses.ToArray();

            Saving.RaiseEvent(new SaveEventArgs<IGeocodedAddress>(geocodedAddresses), this);            


            using (new WriteLock(Locker))
            {
                var uow = _uowProvider.GetUnitOfWork();

                using (var repository = _repositoryFactory.CreateGeocodedAddressRepository(uow))
                {
                    foreach (var address in geocodedAddresses)
                    {
                        
                        repository.AddOrUpdate((GeocodedAddress)address);    
                    }                    
                    uow.Commit();
                }
            }

            Saved.RaiseEvent(new SaveEventArgs<IGeocodedAddress>(geocodedAddresses), this);
        }


        public void Delete(IGeocodedAddress address)
        {
            Deleting.RaiseEvent(new DeleteEventArgs<IGeocodedAddress>(address), this);

            using (new WriteLock(Locker))
            {
                var uow = _uowProvider.GetUnitOfWork();
                using (var repository = _repositoryFactory.CreateGeocodedAddressRepository(uow))
                {
                    repository.Delete((GeocodedAddress)address);
                    uow.Commit();
                }
            }

            Deleted.RaiseEvent(new DeleteEventArgs<IGeocodedAddress>(address), this);
        }

        public void Delete(IEnumerable<IGeocodedAddress> addresses)
        {
            var addressArray = addresses as IGeocodedAddress[] ?? addresses.ToArray();
            Deleting.RaiseEvent(new DeleteEventArgs<IGeocodedAddress>(addressArray), this);

            using (new WriteLock(Locker))
            {
                var uow = _uowProvider.GetUnitOfWork();
                using (var repository = _repositoryFactory.CreateGeocodedAddressRepository(uow))
                {
                    foreach (var address in addressArray)
                    {
                        repository.Delete((GeocodedAddress)address);
                    }
                    uow.Commit();
                }
            }
            
            Deleted.RaiseEvent(new DeleteEventArgs<IGeocodedAddress>(addressArray), this);
        }

        public IGeocodedAddress LookupGeocode(IGeocodedAddress address, bool overwrite = false)
        {
            // prevent a geocode lookup on an empty address
            if (!address.AddressCanBeFormatted() ||
                (address.GeocodeStatus == GeocodeStatus.OK && !overwrite)
                ) return address;

            var defaultCoordinate = new Coordinate(0, 0);

            var geoResponse = AbstractGeocodeServiceProvider
                            .Instance()
                            .GetGeocodeResponse(
                                address.Address1,
                                address.Address2,
                                address.Locality,
                                address.Region,
                                address.PostalCode,
                                address.CountryCode
                                );

            address.GeocodeStatus = geoResponse.Status;

            // first results
            var result = geoResponse.Results.FirstOrDefault();

            if (result != null && geoResponse.Status == GeocodeStatus.OK)
            {
                var coordinate = new Coordinate(result.Latitude, result.Longitude);
                address.Coordinate = coordinate;
                address.Viewport = result.Viewport;
            }
            else
            {
                address.Coordinate = defaultCoordinate;
                address.Viewport = new Viewport(defaultCoordinate, new Coordinate(1, 1));
            }

            return address;
        }

        public IEnumerable<IGeocodedAddress> LookupGeocode(IEnumerable<IGeocodedAddress> addresses, bool overwrite = false)
        {
            return addresses.Select(address => LookupGeocode(address, overwrite));
        }

        #region Event Handlers

        /// <summary>
        /// Occurs before Delete
        /// </summary>		
        public static event TypedEventHandler<IGeocodedAddressService, DeleteEventArgs<IGeocodedAddress>> Deleting;

        /// <summary>
        /// Occurs after Delete
        /// </summary>
        public static event TypedEventHandler<IGeocodedAddressService, DeleteEventArgs<IGeocodedAddress>> Deleted;

        /// <summary>
        /// Occurs before Save
        /// </summary>
        public static event TypedEventHandler<IGeocodedAddressService, SaveEventArgs<IGeocodedAddress>> Saving;

        /// <summary>
        /// Occurs after Save
        /// </summary>
        public static event TypedEventHandler<IGeocodedAddressService, SaveEventArgs<IGeocodedAddress>> Saved;

        /// <summary>
        /// Occurs before Create
        /// </summary>
        public static event TypedEventHandler<IGeocodedAddressService, NewEventArgs<IGeocodedAddress>> Creating;

        /// <summary>
        /// Occurs after Create
        /// </summary>
        public static event TypedEventHandler<IGeocodedAddressService, NewEventArgs<IGeocodedAddress>> Created;



        #endregion

    }
}
