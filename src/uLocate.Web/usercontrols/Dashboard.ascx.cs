﻿using System;
using System.Linq;
using System.Web.UI;
using ServiceStack.Text;
using uLocate.Core;
using uLocate.Core.Models;

namespace uLocate.Web.UserControls
{
    public partial class Dashboard : UserControl
    {
        public string JsonAddresses { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var addresses = ULocateHelper.GeocodedAddressList().Where(x => x.GeocodeStatus == GeocodeStatus.OK);
            if (addresses != null && addresses.Any())
            {
                var array = JsonSerializer.SerializeToString(addresses);
                this.JsonAddresses = string.Concat("var addresses = ", array, ";");
            }
        }
    }
}