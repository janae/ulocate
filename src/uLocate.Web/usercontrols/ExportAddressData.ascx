﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExportAddressData.ascx.cs" Inherits="uLocate.Web.UserControls.ExportAddressData" %>

<div class="dashboardWrapper">
    <h2>Export Addresses</h2>
    <img src="Dashboard/Images/logo32x32.png" alt="logo" class="dashboardIcon" />

    <asp:PlaceHolder runat="server" ID="phError" Visible="false">
        <p>Unable to connect to the uLocate database.</p>
    </asp:PlaceHolder>

    <asp:PlaceHolder runat="server" ID="phExport" Visible="true">
        <p>
            <span>Select from the options, then press the <strong>"Export Addresses"</strong> button to generate and download.</span>
        </p>
        <p>
            <div>
                <asp:RadioButtonList runat="server" ID="rblFormat">
                    <asp:ListItem Text="CSV" Value="CSV" Selected="True" />
                    <asp:ListItem Text="JSON" Value="JSON" />
                    <asp:ListItem Text="XML" Value="XML" />
                </asp:RadioButtonList>
            </div>
            <div>
                <asp:CheckBox runat="server" ID="cbZip" Text="Zip compress export file" Checked="true" />
            </div>
        </p>
        <p>
            <asp:Button runat="server" ID="btnExport" OnClick="btnExport_Click" Text="Export Addresses" />
        </p>
    </asp:PlaceHolder>
</div>
