﻿using System;
using System.Linq;
using uLocate.Core;
using Umbraco.Web.UI.Pages;

namespace uLocate.Web.WebPages
{
    public partial class ViewGeocodedAddresses : UmbracoEnsuredPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var letter = Request.QueryString["letter"];
            if (!string.IsNullOrWhiteSpace(letter))
            {
                var addresses = ULocateHelper.GeocodedAddressList();
                var addressSet = string.Equals(letter, "other", StringComparison.InvariantCultureIgnoreCase) ?
                    addresses.Where(n => !Char.IsLetter(n.Name[0])).OrderBy(n => n.Name) :
                    addresses.Where(n => n.Name.StartsWith(letter, StringComparison.InvariantCultureIgnoreCase)).OrderBy(n => n.Name);

                rpt.DataSource = addressSet;
                rpt.DataBind();
            }
        }
    }
}