﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using uLocate.Core;
using umbraco.BusinessLogic;
using umbraco.cms.businesslogic.packager.standardPackageActions;
using umbraco.interfaces;
using Umbraco.Core.IO;
using Umbraco.Core.Logging;

namespace uLocate.Web.PackageActions
{
    /// <summary>
    /// This package action will create the database table for uLocate.
    /// </summary>
    public class CreateDatabase : IPackageAction
    {
        /// <summary>
        /// This Alias must be unique and is used as an identifier that must match the alias in the package action XML.
        /// </summary>
        /// <returns>The Alias of the package action.</returns>
        public string Alias()
        {
            return string.Concat(Constants.ApplicationName, "_CreateDatabase");
        }

        /// <summary>
        /// Executes the specified package name.
        /// </summary>
        /// <param name="packageName">Name of the package.</param>
        /// <param name="xmlData">The XML data.</param>
        /// <returns></returns>
        public bool Execute(string packageName, XmlNode xmlData)
        {
            try
            {
                // Get the CREATE script, run it against the database
                var path = IOHelper.MapPath("~/umbraco/plugins/uLocate/uLocate_Create.sql");
                if (File.Exists(path))
                {
                    var sql = File.ReadAllText(path);
                    if (!string.IsNullOrWhiteSpace(sql))
                    {
                        Application.SqlHelper.ExecuteNonQuery(sql);
                        return true;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                var message = string.Concat("Error at install ", this.Alias(), " package action: ", ex);
                LogHelper.Error(typeof(AddConfigSection), message, ex);
            }

            return false;
        }

        /// <summary>
        /// Returns a Sample XML Node
        /// </summary>
        /// <returns>The sample xml as node</returns>
        public XmlNode SampleXml()
        {
            var xml = string.Concat("<Action runat=\"install\" undo=\"true\" alias=\"", this.Alias(), "\" />");
            return helper.parseStringToXmlNode(xml);
        }

        /// <summary>
        /// Undoes the specified package name.
        /// </summary>
        /// <param name="packageName">Name of the package.</param>
        /// <param name="xmlData">The XML data.</param>
        /// <returns></returns>
        public bool Undo(string packageName, XmlNode xmlData)
        {
            try
            {
                // TODO: Get the DROP script, run it against the database.
                var path = IOHelper.MapPath("~/umbraco/plugins/uLocate/uLocate_Drop.sql");
                if (File.Exists(path))
                {
                    var sql = File.ReadAllText(path);
                    if (!string.IsNullOrWhiteSpace(sql))
                    {
                        Application.SqlHelper.ExecuteNonQuery(sql);
                        return true;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                var message = string.Concat("Error at undo ", this.Alias(), " package action: ", ex);
                LogHelper.Error(typeof(AddConfigSection), message, ex);
            }

            return false;
        }
    }
}