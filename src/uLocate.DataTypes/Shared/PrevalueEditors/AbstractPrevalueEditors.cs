﻿using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;

using umbraco.interfaces;
using umbraco.editorControls;
using umbraco.cms.businesslogic.datatype;

using uLocate.Core;


namespace uLocate.DataTypes.Shared.PrevalueEditors
{
    
    /// <summary>
    /// Abstract class for the PreValue Editor.
    /// </summary>
    /// <remarks>
    /// Modified versoin of uComponents AbstractPrevalueEditor
    /// http://ucomponents.codeplex.com/license
    /// </remarks>
    public abstract class AbstractPrevalueEditor : WebControl
    {

        #region Fields

        /// <summary>
        /// The underlying base data-type.
        /// </summary>
        protected umbraco.cms.businesslogic.datatype.BaseDataType m_DataType;

        /// <summary>
        /// The list of editor prevalues
        /// </summary>
        protected SortedList m_PreValues;

        /// <summary>
        /// Flag indicating whether or not there was an error setting up the controls
        /// </summary>
        protected bool m_HasError;

        /// <summary>
        /// Error message(s) from exceptions
        /// </summary>
        protected string m_ErrorMessage;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractPrevalueEditor"/> class.
        /// </summary>
        public AbstractPrevalueEditor() : base() { }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the editor.
        /// </summary>
        /// <value>The editor.</value>
        public virtual Control Editor { get { return this; } }

        /// <summary>
        /// Gets the editor pre values
        /// </summary>
        /// <value>The editor pre values.</value>
        protected SortedList EditorPreValues
        {
            get { return m_PreValues ?? (m_PreValues = PreValues.GetPreValues(m_DataType.DataTypeDefinitionId)); }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the pre value
        /// </summary>
        /// <param name="index">0 based index of the value</param>
        protected string GetPreValue(int index)
        {
            return ((PreValue)EditorPreValues[index]).Value;
        }

        /// <summary>
        ///  Inserts the value.
        /// </summary>
        /// <param name = "index">The index.</param>
        /// <param name = "value">The value.</param>
        protected void UpdatePreValue(int index, string value)
        {
            if (EditorPreValues.Count >= index + 1)
            {
                //update
                var preValue = (PreValue)EditorPreValues[index];
                preValue.Value = value;
                preValue.Save();
            }
            else
            {
                //insert
                var preValue = PreValue.MakeNew(m_DataType.DataTypeDefinitionId, value);
                preValue.Save();
            }
            return;
        }

        /// <summary>
        ///   Gets or sets the error message.
        /// </summary>
        /// <value>The error message.</value>
        protected void SetErrorMessage(Exception exception)
        {
            if (exception == null)
            {
                m_HasError = false;
                m_ErrorMessage = string.Empty;
            }
            else
            {
                m_HasError = true;
                m_ErrorMessage += string.Format("<p><b>{0}</b></p><p>{1}</p><p>{2}</p>", exception.Message,
                                               exception.StackTrace, exception.TargetSite);
            }
        }

        #endregion


        
        #region Overrides

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.EnsureChildControls();
            
            //// embed the css file
            //this.RegisterEmbeddedClientResource(Constants.PrevalueEditorCssResourcePath, ClientDependencyType.Css);
        }


        /// <summary>
        /// Renders the HTML opening tag of the control to the specified writer. This method is used primarily by control developers.
        /// </summary>
        /// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, Constants.ApplicationName);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            base.RenderBeginTag(writer);
        }

        /// <summary>
        /// Renders the HTML closing tag of the control into the specified writer. This method is used primarily by control developers.
        /// </summary>
        /// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
            base.RenderEndTag(writer);

            writer.RenderEndTag();
        }

        #endregion

    }
}
