﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using uLocate.Core;
using uLocate.Core.Extensions;
using umbraco;

namespace uLocate.DataTypes.GeocodedAddress
{
    [ValidationProperty("IsValid")]
    public class GeocodedAddressControl : Panel
    {
        private HiddenField SelectedValue;
        private TextBox Name;
        private TextBox Address1;
        private TextBox Address2;
        private TextBox Locality;
        private TextBox Region;
        private TextBox PostalCode;
        private DropDownList CountryCode;

        private TextBox Telephone;
        private TextBox Fax;
        private TextBox Email;
        private TextBox WebsiteUrl;
        private TextBox Comment;

        public string IsValid
        {
            get
            {
                if (this.SelectedValue != null && !string.IsNullOrEmpty(this.SelectedValue.Value) && this.SelectedValue.Value != "-1")
                {
                    return "Valid";
                }

                return string.Empty;
            }
        }

        public GeocodedAddressOptions Options { get; set; }

        public int AddressId
        {
            get
            {
                var addressId = -1;
                if (this.SelectedValue != null && int.TryParse(this.SelectedValue.Value, out addressId))
                {
                    return addressId;
                }

                return addressId;
            }
            set
            {
                this.SelectedValue.Value = value.ToString();
            }
        }

        public GeocodedAddressControl()
        {
            this.CssClass = Constants.ApplicationName;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.EnsureChildControls();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!this.Page.IsPostBack)
            {
                var address = GetGeocodedAddress(this.AddressId);
                this.Name.Text = address.Name;
                this.Address1.Text = address.Address1;
                this.Address2.Text = address.Address2;
                this.Locality.Text = address.Locality;
                this.Region.Text = address.Region;
                this.PostalCode.Text = address.PostalCode;
                this.CountryCode.SelectedValue = address.CountryCode;

                this.Telephone.Text = address.Telephone;
                this.Fax.Text = address.Fax;
                this.Email.Text = address.Email;
                this.WebsiteUrl.Text = address.WebsiteUrl;
                this.Comment.Text = address.Comment;
            }
        }

        protected override void CreateChildControls()
        {
            base.CreateChildControls();

            try
            {
                // create the controls
                this.SelectedValue = new HiddenField() { ID = "SelectedValue" };
                this.Name = new TextBox() { ID = "Name", CssClass = "umbEditorTextField" };
                this.Address1 = new TextBox() { ID = "Address1", CssClass = "umbEditorTextField" };
                this.Address2 = new TextBox() { ID = "Address2", CssClass = "umbEditorTextField" };
                this.Locality = new TextBox() { ID = "Locality", CssClass = "umbEditorTextField" };
                this.Region = new TextBox() { ID = "Region", CssClass = "umbEditorTextField" };
                this.PostalCode = new TextBox() { ID = "PostalCode", CssClass = "umbEditorTextField" };

                this.CountryCode = new DropDownList() { ID = "CountryCode", CssClass = "umbEditorTextField", DataSource = ULocateHelper.CountryCodes(), DataTextField = "key", DataValueField = "value" };
                this.CountryCode.DataBind();
                this.CountryCode.Items.Insert(0, new ListItem(string.Concat(ui.Text("choose"), "..."), string.Empty));
                if (!string.IsNullOrEmpty(Constants.Configuration.DefaultCountryCode))
                    this.CountryCode.SelectedValue = Constants.Configuration.DefaultCountryCode;

                this.Telephone = new TextBox() { ID = "Telephone", CssClass = "umbEditorTextField" };
                this.Fax = new TextBox() { ID = "Fax", CssClass = "umbEditorTextField" };
                this.Email = new TextBox() { ID = "Email", CssClass = "umbEditorTextField" };
                this.WebsiteUrl = new TextBox() { ID = "WebsiteUrl", CssClass = "umbEditorTextField" };
                this.Comment = new TextBox() { ID = "Comment", CssClass = "umbEditorTextField", TextMode = TextBoxMode.MultiLine };

                // add the controls
                this.Controls.Add(this.SelectedValue);
                if (this.Options.ShowNameField)
                    this.AddControl("Name", this.Name);
                this.AddControl("Address 1", this.Address1);
                this.AddControl("Address 2", this.Address2);
                this.AddControl("Locality", this.Locality);
                this.AddControl("Region", this.Region);
                this.AddControl("Postal Code", this.PostalCode);
                this.AddControl("Country", this.CountryCode);

                this.AddControl(string.Empty, new HtmlGenericControl("hr"));

                this.AddControl("Telephone", this.Telephone);
                this.AddControl("Fax", this.Fax);
                this.AddControl("Email", this.Email);
                this.AddControl("Website Url", this.WebsiteUrl);
                this.AddControl("Comment", this.Comment);
            }
            catch (Exception ex)
            {
                // display an error message.
                this.Controls.Add(new LiteralControl("<em>The corresponding addresses does not exist.</em>"));
                this.Controls.Add(new LiteralControl(string.Concat("<div style='display:none;white-space:pre;width:500px;'><small>", ex, "</small></div>")));
            }
        }

        public int Save()
        {
            uLocate.Core.Models.GeocodedAddress address = GetGeocodedAddress(this.AddressId);

            if (this.Options.ShowNameField)
            {
                address.Name = string.IsNullOrEmpty(this.Name.Text) ? this.GetCurrentNodeName() : this.Name.Text;
            }
            else
            {
                address.Name = this.GetCurrentNodeName();
            }

            address.Address1 = this.Address1.Text;
            address.Address2 = this.Address2.Text;
            address.Locality = this.Locality.Text;
            address.Region = this.Region.Text;
            address.PostalCode = this.PostalCode.Text;
            address.CountryCode = this.CountryCode.SelectedValue;

            address.Telephone = this.Telephone.Text;
            address.Fax = this.Fax.Text;
            address.Email = this.Email.Text;
            address.WebsiteUrl = this.WebsiteUrl.Text;
            address.Comment = this.Comment.Text;

            // address.GeocodeStatus = GeocodeStatus.NOT_QUERIED;

            address.Save();

            return address.Id;
        }

        private void AddControl(string text, Control control)
        {
            var label = new Label() { AssociatedControlID = control.ID, Text = text };

            // add the item container
            var propertyItem = new HtmlGenericControl("div");
            propertyItem.Attributes.Add("class", "propertyItem");
            propertyItem.Attributes.Add("style", "width: 556px;");

            // add the header
            var propertyItemHeader = new HtmlGenericControl("div");
            propertyItemHeader.Attributes.Add("class", "propertyItemheader");
            propertyItemHeader.Attributes.Add("style", "padding-top: 7px;");
            propertyItemHeader.Controls.Add(label);

            // add the content/control
            var propertyItemContent = new HtmlGenericControl("div");
            propertyItemContent.Attributes.Add("class", "propertyItemContent");
            propertyItemContent.Attributes.Add("style", "width: 400px;");
            propertyItemContent.Controls.Add(control);

            propertyItem.Controls.Add(propertyItemHeader);
            propertyItem.Controls.Add(propertyItemContent);

            this.Controls.Add(propertyItem);
        }

        private uLocate.Core.Models.GeocodedAddress GetGeocodedAddress(int addressId)
        {
            var emptyAddress = new uLocate.Core.Models.GeocodedAddress() { Name = this.GetCurrentNodeName() };

            if (addressId == 0)
                return emptyAddress;

            var address = ULocateHelper.GeocodedAddress(addressId);

            return (Core.Models.GeocodedAddress) (object.ReferenceEquals(address, null) ?
                emptyAddress :
                address);
        }

        private string GetCurrentNodeName()
        {
            int nodeId;
            var id = uQuery.GetIdFromQueryString();

            if (int.TryParse(id, out nodeId))
            {
                // using `Content` as this data-type could be used across content, media or members.
                var node = new umbraco.cms.businesslogic.Content(nodeId);
                return node.Text;
            }

            // fallback on concatenating the address's Id and first line.
            return string.Concat(this.AddressId, " - ", this.Address1.Text);
        }
    }
}
