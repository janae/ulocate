uLocate

Geography data types for Umbraco based on the SqlGeography type.

This package will only work using a SQL Server database.

Thanks to Lee Kelleher for package development guidance!